<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumeSkillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resume_skill', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resume_id')->unsigned()->index();
            $table->integer('skill_id')->unsigned()->index();

            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->foreign('resume_id')->references('id')->on('resumes')->onDelete('cascade');;
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resume_skill');
    }
}
