<?php

use App\Skill;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class Skills extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = array("Java/ JavaEE/ Spring Framework", "Python/ Numpy/ Scipy", "C/ C++");
        foreach ($skills as $s) {
            Skill::create([
                'name'        => $s,
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ]);
        }
    }
}
