@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Resume Form</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('resume.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Full Name</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="job" class="col-md-2 col-form-label text-md-right">Jobs</label>

                            <div class="col-md-8">
                                <input id="job" type="text" class="form-control{{ $errors->has('job') ? ' is-invalid' : '' }}" name="job" value="{{ old('job') }}" required>

                                @if ($errors->has('job'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('job') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="work_at" class="col-md-2 col-form-label text-md-right">Work At</label>

                            <div class="col-md-8">
                                <input id="work_at" type="text" class="form-control{{ $errors->has('work_at') ? ' is-invalid' : '' }}" name="work_at" value="{{ old('work_at') }}" required>

                                @if ($errors->has('work_at'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('work_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-2 col-form-label text-md-right">Phone</label>

                            <div class="col-md-8">
                                <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right">Email</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="experience" class="col-md-2 col-form-label text-md-right">Experience</label>

                            <div class="col-md-8">

                                <textarea id="experience" name="experience" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" rows="8" cols="80" required>{{ old('email') }}</textarea>

                                @if ($errors->has('experience'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('experience') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="skill_id[]" class="col-md-2 col-form-label text-md-right">Skills</label>

                            <div class="col-md-8">

                                @foreach ($skills as $skill)
                                  <input id="skill_id[]" type="checkbox" class="{{ $errors->has('skill_id') ? ' is-invalid' : '' }}" name="skill_id[]" value="{{ $skill->id }}"> <span>{{ $skill->name }}</span> <br/>
                                @endforeach

                                @if ($errors->has('skill_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('skill_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
