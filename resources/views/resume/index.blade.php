@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
  <div class="col-md-10">
    <div class="row mb-3 jumbotron">
      <div class="col-md-4">
        <figure class="figure">
            <img src="{{ asset('image/people-m.png') }}" class="figure-img img-fluid rounded-circle" width="150px" alt="Set your avatar">
        </figure>
      </div>
      <div class="col-md-8">
        <ul class="list-group">
            <li class="list-group-item">{{ $resume->name }}</li>
            <li class="list-group-item">{{ $resume->job }}</li>
            <li class="list-group-item">{{ $resume->work_at }}</li>
            <li class="list-group-item">{{ $resume->phone }}</li>
            <li class="list-group-item">{{ $resume->email }}</li>
        </ul>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <blockquote class="blockquote">
              <p class="mb-0">{{ $resume->experience }}</p>
            </blockquote>
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-md-12">
        <p>Skills</p>
        @foreach ($resume->skills as $skill)
          <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">{{ $skill->name }}</div>
          </div>
          <br>
        @endforeach
      </div>
    </div>
  </div>
</div>
</div>
@endsection
