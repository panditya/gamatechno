<?php

namespace App\Http\Controllers;

use App\Resume;
use App\Skill;
use Illuminate\Http\Request;
use Validator;

class ResumeController extends Controller
{

    /**
    * Index view of the resume.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(){

      $resume = Resume::first();

      return view('resume.index', compact('resume'));
    }

    /**
    * View to create new resume.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(){

      $skills = Skill::all();

      return view('resume.create', compact('skills'));
    }

    /**
     * Create a new resume instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request){

      $validator = Validator::make($request->all(), [
          'name'        => 'required',
          'job'         => 'required',
          'work_at'     => 'required',
          'phone'       => 'required',
          'email'       => 'required',
          'experience'  => 'required',
          'skill_id'    => 'required'
      ]);

      if ($validator->fails()) {
          return redirect()->back()
                      ->withErrors($validator)
                      ->withInput();
      }

      $resume = new Resume;
      $resume->name        = $request->name;
      $resume->job         = $request->job;
      $resume->work_at     = $request->work_at;
      $resume->phone       = $request->phone;
      $resume->email       = $request->email;
      $resume->experience  = $request->experience;

      if ($resume->save()) {
        $skill_id   = $request->skill_id;
        $attach     = Resume::findOrFail($resume->id);
        $attach->skills()->attach($skill_id);

        return redirect()->route('resume.index');
      }
    }
}
