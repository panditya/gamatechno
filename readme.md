
## Tes GamaTechno 2018

- Ekstrak file
- Buat database di MySQL
- Edit file .env, ganti nama database, dan data kredensial database. Apabila belum ada copy / rename dari .env.example

Jalankan menggunakan terminal / cmd :

- composer install
- php artisan key:generate
- php artisan migrate
- php artisan db:seed
- php artisan serve

Lalu buka dibrowser http://localhost:8000
